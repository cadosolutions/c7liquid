library c7liquid;

const int START_TAG_CHAR = 123; //'{'.codeUnitAt(0);
const int END_TAG_CHAR = 125; //'}'.codeUnitAt(0);
const int VARIABLE_CHAR = 64; //'@'.codeUnitAt(0);
const int GETTER_CHAR = 46; //'.'.codeUnitAt(0);
const int ESCAPE_CHAR = 92; //'\\'.codeUnitAt(0);
const int STRING_DELIMETER = 39; //'\''.codeUnitAt(0);

enum TokenType {
  Tag, //if else /if
  Operator, //including '+' '+=' '(', '?', '.'
  InlineOp, //@
  Text,
  Identifier, // foo
  StringLiteral,
  IntegerLiteral,
  FloatLiteral
}

/**
* This Tokenizer is aware about tags and inline variables.
* It iterates expressions inside tags and variables and preserves everything else as text nodes.
*/
class Tokenizer implements Iterator<String> {

  Iterator<int> source;

  String current = null;
  TokenType currentType = null;
  bool hasNext = true;
  bool expectTag = false;
  bool inInline = false;
  bool inLongInline = false;

  Tokenizer.fromString(String source) {
    this.source = source.runes.iterator;
  }

  String _consumeText() {
    var runes = new List();
    while (hasNext) {
      if (source.current == VARIABLE_CHAR) {
        break;
      }
      if (source.current == START_TAG_CHAR) {
        hasNext = source.moveNext();
        expectTag = true;
        break;
      }
      if (source.current == ESCAPE_CHAR) {
        hasNext = source.moveNext();
      }
      runes.add(source.current);
      hasNext = source.moveNext();
    }
    return new String.fromCharCodes(runes);
  }

  String _consumeTag() {
    expectTag = false;
    var runes = new List();
    runes.add(source.current);
    while(hasNext = source.moveNext()) {
      if (!_isAtAlpha()) {
        break;
      }
      runes.add(source.current);
    }
    return new String.fromCharCodes(runes);
  }

  String _consumeStringLiteral() {
    var runes = new List();
    while(hasNext = source.moveNext()) {
      if (source.current == STRING_DELIMETER) {
        hasNext = source.moveNext();
        break;
      }
      if (source.current == ESCAPE_CHAR) {
        hasNext = source.moveNext();
      }
      runes.add(source.current);
    }
    return new String.fromCharCodes(runes);
  }

  String _consumeIdentifier() {
    var runes = new List();
    runes.add(source.current);
    while((hasNext = source.moveNext()) && (_isAtAlpha() || _isAtDigit())) {
      runes.add(source.current);
    }
    return new String.fromCharCodes(runes);
  }

  String _consumeNumericLiteral() {
    var runes = new List();
    runes.add(source.current);
    bool dot = false;
    while ((hasNext = source.moveNext()) && (_isAtDigit() || (source.current == '.'.codeUnitAt(0)))) {
      if (source.current == '.'.codeUnitAt(0)) {
        currentType = TokenType.FloatLiteral;
        if (!dot) dot = true; else break;
      }
      runes.add(source.current);
    }
    return new String.fromCharCodes(runes);
  }

  bool _isAtAlpha() {
    if (source.current >= 'a'.codeUnitAt(0) && source.current <= 'z'.codeUnitAt(0)) {
      return true;
    } else if (source.current >= 'A'.codeUnitAt(0) && source.current <= 'Z'.codeUnitAt(0)) {
      return true;
    } else {
      return false;
    }
  }

  bool _isAtWhitespace() {
    return ' \t\n'.codeUnits.contains(source.current);
  }

  bool _isAtDigit() {
    return source.current >= '0'.codeUnitAt(0) && source.current <= '9'.codeUnitAt(0);
  }

  bool moveNext() {
    if (!hasNext) {
        return false;
    }
    if (expectTag) {
        currentType = TokenType.Tag;
        current = _consumeTag();
        return true;
    }


    if (inInline) {
      if (_isAtAlpha()) {
        currentType = TokenType.Identifier;
        current = _consumeIdentifier();
        return true;
      } else if (source.current == GETTER_CHAR){
        currentType = TokenType.Operator;
        current = new String.fromCharCodes([source.current]);
        hasNext = source.moveNext();
        return true;
      } else {
        inInline = false;
        currentType = TokenType.Text;
        current = _consumeText();
        return current.length > 0 ? true : moveNext();
      }
    }
    int first = source.current == null ? END_TAG_CHAR : source.current;
    switch (first) {
      case END_TAG_CHAR:
        if (inLongInline){
          currentType = TokenType.InlineOp;
          current = new String.fromCharCodes([source.current]);
          inLongInline = false;
          return true;
        }
        hasNext = source.moveNext();
        currentType = TokenType.Text;
        current = _consumeText();
        return current.length > 0 ? true : moveNext();
      case STRING_DELIMETER:
        currentType = TokenType.StringLiteral;
        current = _consumeStringLiteral();
        return true;
      case VARIABLE_CHAR:
        currentType = TokenType.InlineOp;
        var runes = [source.current];
        if(hasNext = source.moveNext()){
          if (source.current != START_TAG_CHAR) {
            inInline = true;
          } else {
            runes.add(source.current);
            hasNext = source.moveNext();
            inLongInline = true;
          }
        }
        current = new String.fromCharCodes(runes);
        return true;
      default:
        if (_isAtDigit()) {
            currentType = TokenType.IntegerLiteral;
            current = _consumeNumericLiteral();
        } else if (_isAtAlpha()) {
            currentType = TokenType.Identifier;
            current = _consumeIdentifier();
        } else if (_isAtWhitespace()) {
            while ((hasNext = source.moveNext()) && _isAtWhitespace());
            return moveNext();
        } else {
            currentType = TokenType.Operator;
            current = new String.fromCharCodes([source.current]);
            hasNext = source.moveNext();
        }
        return true;
    }
  }
}
