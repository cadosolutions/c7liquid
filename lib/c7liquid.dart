library c7liquid;

import 'dart:mirrors';
import 'dart:io';
import 'dart:async';
import 'dart:convert';

class Teardrop {

}
/**
 * decorator for renderable fields
 */
class _Liquid { const _Liquid(); }
const Liquid = const _Liquid();

/**
 * this is a template that can chacnge any instance of T to String
 */
class LiquidTemplate<T> {
    String body;
    LiquidTemplate parent;
    LiquidTemplate(this.body, this.parent){

    }
    String render(T object){
        return "TODO";
    }
}

abstract class LiquidTemplateLoader {
  Future<LiquidTemplate> getTemplateForClass(String name);
}

class FileSystemTemplateLoader {
  String templatesRoot;
  FileSystemTemplateLoader(this.templatesRoot);

  Future<String> getTemplateForClass(String name) async {
    var file = new File('$name.liquid');
    return await (file.readAsString(encoding: UTF8));
  }

  Future<LiquidTemplate<T>> getForType(T) async {
    //return new
  }

}

abstract class ITemplate {
  TypeMirror get dataType;
  ITemplate get baseTemplate;
  String get templateBody;
}

class StringTemplate implements ITemplate {
  StringTemplate baseTemplate = null;
  TypeMirror dataType;
  String templateBody;
  StringTemplate(this.templateBody, Type type) {
    this.dataType = reflectType(type);
  }
}

class FileTemplate implements ITemplate {
  String path;
  String templateBody;
  TypeMirror dataType;
  FileTemplate baseTemplate = null;

  FileTemplate._internal(this.path);

  static Future<FileTemplate> getFromPath(String path) async {
    var ret = new FileTemplate._internal(path);
    var file = new File('$path.liquid');
    ret.templateBody = await (file.readAsString(encoding: ASCII));
    print(path);

    var match = ret.templateBody.allMatches(r"extends");
    print(ret.templateBody);
    print(match);
    return ret;
  }
}

class LiquidSyntaxException implements Exception {
  int line;
  int char;
  String message;
  LiquidSyntaxException(this.line, this.char, this.message);
}

class LiquidRenderException implements Exception {
  int line;
  int char;
  String message;
  LiquidRenderException(this.line, this.char, this.message);
}

abstract class Node {
  String tagName = null;
  Node parent;
  List<dynamic> args = new List<dynamic>();
  List<Node> subnodes = new List<Node>();

  dynamic evaluate(Expression exp, [Map<Symbol, dynamic> extraVars = null]){
    return parent.evaluate(exp, extraVars);
  }

  /* dynamic evaluateExpression(String name, [dynamic context = null]){
    return parent.evaluateExpression(name, context);
  } */

  String get render {
    return subnodes.map((e)=>e.render).join();
  }

  dynamic evaluateArgument(String arg, int idx){
    return evaluate(new Expression.fromString(arg));
  }
}

abstract class Tag extends Node{
}

abstract class ShortTag extends Node {
}

class TextNode extends Node {
  String content;
  TextNode(this.content);
  //TODO
  String get render => this.content;
}

class VariableNode extends Node {
  Expression expression;
  VariableNode(String source){
    expression = new Expression.fromString(source);
  }
  String get render => this.parent.evaluate(expression);
}


class _StringExt {
    static bool matches(String haystack, String needle, int i, [bool escapeable = false]) {
        bool match = true;
        for (int j = 0; j < needle.length; j ++) {
          match = match && (haystack[i+j] == needle[j]);
        }
        if (match && escapeable) {
          int c = 0; //escape characters count
          while (i - c > 0 && haystack[i - c - 1] == '\\') c++;
          return c.isEven;
        }
        return match;
    }
}

class RootNode extends Node {
  InstanceMirror data;

  @override
  dynamic evaluate(Expression expression, [Map<Symbol, dynamic> extraVars = null]) {
    return expression.evaluate(data, extraVars);
  }
  /*
  @override
  dynamic evaluateExpression(String expression, [dynamic context = null]){
    String symbol = '';
    var field = context != null ? context : template;
    for (int i =0; i < expression.length; i++){
      if (expression[i]=='.'){
        field = template.getField(new Symbol(symbol));
        symbol = '';
      } else if (expression[i]=='(') {
        if (expression[expression.length - 1] != ')') throw new Exception('unterminated (');
        String args = expression.substring(i + 1, expression.length - 1);
        //TODO multi args comma separated print("ARGS<" + args + ">");
        //return field.getField(new Symbol(symbol)).invoke(new Symbol('()'), [evaluateExpression(args)]).reflectee;
        return field.invoke(new Symbol(symbol), [evaluateExpression(args)]).reflectee;
        break;
      } else if (expression[i]=='>') {
        String operand = expression.substring(i + 1);
        return field.getField(new Symbol(symbol)).invoke(new Symbol('>'), [evaluateExpression(operand)]).reflectee;
      } else {
        symbol += expression[i];
      }
    }
    double literal = double.parse(symbol, (e) => null);
    if (literal != null)
      return literal;
    //return "X:" + symbol;
    return field.getField(new Symbol(symbol)).reflectee;
  } */
}

class DumpTag extends ShortTag {
  String get render {
    //TODO loop all args and print all LiquidProperties checking for reference loops
    //TODO allow this tag only in some testing mode?
    return "<pre>TODO</pre>";
  }
}


class IfTag extends Tag {

  String get render {
    if (args.length != 1) {
        throw new Exception('if shall take exactly one arg not ${args.length}');
    }
    bool shallRender = args[0];
    String ret = '';
    for (var node in subnodes) {
      if (node is ElseTag) shallRender = !shallRender;
      else if (shallRender) ret += node.render;
    }
    return ret;
  }
}
class ElseTag extends ShortTag {
  String get render {
    throw new Exception('syntax error');
  }
}

class ElseIfTag extends ShortTag {
  String get render {
    throw new Exception('syntax error');
  }
}

class ForTag extends Tag {
  Symbol loopKey;
  var loopVar;

  dynamic evaluateArgument(String arg, int idx){
    if (idx == 0) {
      loopKey = new Symbol(arg);
      return null;
    } else if (idx == 1) {
      if (arg != 'in')
        throw new Exception('for syntax is {for VAR in ITERABLE}');
      return null;
    } else {
      return super.evaluateArgument(arg, idx);
    }
  }

  @override
  dynamic evaluate(Expression expression, [Map<Symbol, dynamic> extraVars = null]) {
    if (extraVars == null) extraVars = new Map<Symbol, dynamic>();
    extraVars[loopKey] = loopVar;
    return parent.evaluate(expression, extraVars);
  }

  /*
  @override
  dynamic evaluateExpression(String expression, [dynamic context = null]){
    if (context == null && expression == 'in')
      return inTag;
    if (expression == loopKey)
      return loopVar;
    if ((context == null) && expression.startsWith(loopKey))
      return super.evaluateExpression(expression.substring(loopKey.length + 1), reflect(loopVar));
    return super.evaluateExpression(expression);
  } */

  String get render {
    if (args.length != 3) {
        throw new Exception('for syntax is {for VAR in ITERABLE}');
    }
    return args[2].map(
      (element) {
        loopVar = element;
        return subnodes.map((e)=>e.render).join();
      }
    ).join();
  }
}

enum _ParserState {
  OUTSIDE,
  IN_OPENING_TAG,
  IN_TAG_ARGUMENT,
  IN_CLOSING_TAG,
  IN_VARIABLE
}

class Renderer {
  static String escapeChar = "\\";
  static String tagOpenChar = "{";
  static String tagCloseChar = "}";
  static String tagEndChar = "/";
  static String variableChar = "@";

  static Node tagFactory(String name) {
    switch(name){
      case 'dump': return new DumpTag();
      case 'if': return new IfTag();
      case 'else': return new ElseTag();
      case 'for': return new ForTag();
      default:
        throw new Exception('Tag $name not found');
      }
  }

  ITemplate template;

  Renderer(this.template);

  //HashMap vars

  String render(var data) {
    RootNode ret = new RootNode();
    ret.data = reflect(data);
    _ParserState state = _ParserState.OUTSIDE;
    String tagName = '';
    List<String> arguments = new List<String>();
    String argument = '';
    String textNode = '';
    String expression = '';
    RegExp varRegExp = new RegExp(r"[a-zA-Z0-9\.\(\)]");
    //int state =;
    for (int i=0; i<template.templateBody.length; i++) {
      if ((state == _ParserState.IN_VARIABLE) && (!varRegExp.hasMatch(template.templateBody[i]))){
        if (textNode != '') {
          ret.subnodes.add(new TextNode(textNode));
          textNode = '';
        }
        var node = new VariableNode(expression);
        node.parent = ret;
        ret.subnodes.add(node);
        expression = '';
        state = _ParserState.OUTSIDE;
      }

      if ((state == _ParserState.OUTSIDE) && (template.templateBody[i] == variableChar)){
        state = _ParserState.IN_VARIABLE;
      } else if (template.templateBody[i] == tagOpenChar){
        if (textNode != '') {
          //TODO dont strip whitespace for shorttags
          ret.subnodes.add(new TextNode(textNode.replaceFirst(new RegExp(r"\s+$"), "")));
          textNode = '';
        }
        state = _ParserState.IN_OPENING_TAG;
      } else if ((state == _ParserState.IN_TAG_ARGUMENT) && (template.templateBody[i] == ' ')){
          arguments.add(argument);
          argument = '';
      } else if (template.templateBody[i] == tagCloseChar){
        if (state == _ParserState.IN_OPENING_TAG || state == _ParserState.IN_TAG_ARGUMENT) {
          if(state == _ParserState.IN_TAG_ARGUMENT){
            arguments.add(argument);
            argument = '';
          }
          var tag = tagFactory(tagName);
          tag.parent = ret;
          tag.args = new List<dynamic>();
          int argIdx = 0;
          for (var arg in arguments){
            tag.args.add(tag.evaluateArgument(arg, argIdx));
            argIdx ++;
          }
          ret.subnodes.add(tag);
          if (!(tag is ShortTag)){
            ret = tag;
          }
          arguments.clear();

        } else if (state == _ParserState.IN_CLOSING_TAG) {
          //ret. tagName);
          ret = ret.parent;
        }
        tagName = '';
        state = _ParserState.OUTSIDE;
      } else if ((state == _ParserState.IN_OPENING_TAG) && (template.templateBody[i] == tagEndChar) && (tagName == '')) {
        state = _ParserState.IN_CLOSING_TAG;
      } else if ((state == _ParserState.IN_OPENING_TAG) && (template.templateBody[i] == ' ')){
        state = _ParserState.IN_TAG_ARGUMENT;
      } else {
        switch(state){
          case _ParserState.IN_VARIABLE:
            expression += template.templateBody[i];
            break;
          case _ParserState.OUTSIDE:
            textNode += template.templateBody[i];
            break;
          case _ParserState.IN_OPENING_TAG:
            tagName += template.templateBody[i];
            break;
          case _ParserState.IN_TAG_ARGUMENT:
            argument += template.templateBody[i];
            break;
          case _ParserState.IN_CLOSING_TAG:
            tagName += template.templateBody[i];
            break;
        }
      }
    }
    ret.subnodes.add(new TextNode(textNode));

    return ret.render;
  }
}
