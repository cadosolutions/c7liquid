library c7liquid;

class Expression {

  Symbol symbol;
  List<Expression> arguments;

  //bool isInvokable;
  //ExpressionChunk(this.symbol, [this.isInvokable = false, this.arguments = null]);
}



class ExpressionVisitor {
    void visitUnaryOperator();
    void visitBinaryOperator();
    void visitTernaryOperator();
    void visitStringLiteral();
    void visitIntegerLiteral();
    void visitFloatLiteral();
    void visitGetter();
}
