library c7liquid;

import 'package:c7liquid/expression.dart';

class Node {
  void visit(TemplateVisitor visitor);
}

class TextNode implements Node {
  String text;

  TextNode(this.text);

  void visit(TemplateVisitor visitor) {
    visitor.visitTextNode(this);
  }
}

class IfPart {
  Expression condition;
  List<Node> children;
}

class IfChainNode implements Node {
  List<IfPart> chain;
  void visit(TemplateVisitor visitor) {
    visitor.visitIfNode(this);
  }
}

class ForNode implements Node {
  Expression iterable;
}

class DefineNode implements Node {
  Expression value;
  List<Node> children;
}

class CommentNode implements Node {
  List<Node> children;
}

class InlineNode extends Node {
  Expression expression;
}

class DumpNode extends Node {
  Expression expression;
}

interface TemplateVisitor() {
    void visitTextNode(TextNode);
    void visitIfNode(IfNode);
    void visitForNode(ForNode);
    void visitDefineNode(DefineNode);
    void visitCommentNode(CommentNode);
    void visitInlineNode(InlineNode);
    void visitDumpNode(DumpNode);
}
