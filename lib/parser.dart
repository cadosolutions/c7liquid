library c7liquid;

import 'package:c7liquid/tokenizer.dart';
import 'package:c7liquid/nodes.dart';

/**
 * parser changes stream of tokens and a type info into tree of nodes.
 */
class Parser<T> {

  Tokenizer _tokens;
  bool hasNext = true;

  Parser(this._tokens);

  void _assert(bool condition) {
    if (!condition) {
      throw new Exception('expected other tag');
    }
  }

  Expression _parseExpression() {
    switch(_tokens.currentType) {
      case TokenType.Identifier:

      case TokenType.Operator:
        switch(_tokens.current) {
          case "(":
            return _parseShortInline();
        }
    }

    
  }

  IfNode _parseIfNode() {
    var tag = new IfChainNode();
    var first = new IfPart();
    first.condition = _parseExpression();
    first.children = _parseChildren();
    tag.chain.add(first);
    while (tokens.moveNext()) {
      if (tokens.currentType )
      var next = new IfPart();
      next.condition = _parseExpression();
      next.children = _parseChildren();
      tag.chain.add(next);
    }
    while (_tokens.current == "else") {
      var next = new IfPart();
      next.condition = "1";
      next.children = _parseChildren();
      tag.chain.add(next);
    }
    _assert(_tokens.current == "/if");
  }

  Expression _parseTagParam() {
    tokens.current;
  }

  Expression _parseInlineExpression() {
    if (_tokens.current == "{") {
      return _parseExpression();
    } else {
      return _parseVariable();
    }
  }

  Node _parseTextNode() {
    var ret = new TextNode(_tokens.current);
    hasNext = _tokens.moveNext();
    return ret;
  }

  Node _parseChild() {
    switch(_tokens.currentType) {
      case TokenType.Text:
        return _parseTextNode();
      case TokenType.InlineOp:
        switch(_tokens.current) {
          case "@{":
            return _parseLongInline();
          case "@":
            return _parseShortInline();
        }
      case TokenType.Tag:
        switch(_tokens.current) {
          case "if":
            return _parseIfNode();
          case "for":
            return _parseForNode();
          case "define":
            return _parseDefineNode();
          case "comment":
            return _parseCommentNode();
        }
    }
    return null;
  }

  List<Node> _parseChildren() {
    List<Node> ret = new List<Node>();
    Node next;
    while ((next = _parseChild()) != null){
      ret.add(next);
    }
    return ret;
  }

  List<Node> parse() {
    hasNext = _tokens.moveNext();
    List<Node> ret = _parseChildren();
    if (hasNext) {
      throw new Exception('unexpected token');
    }
    return ret;
  }

}
