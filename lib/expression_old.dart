library c7liquid;

import 'dart:mirrors';

class LiquidExpressionSyntaxException implements Exception {
  String message;
  String expression;
  int position;
  LiquidExpressionSyntaxException(this.message, this.expression, [this.position = 0]);
  String toString(){
    return this.message + ' in "' + this.expression + '" at char ' + this.position.toString();
  }
}

class _StringExt {
    static bool matches(String haystack, String needle, int i, [bool escapeable = false]) {
        bool match = true;
        for (int j = 0; j < needle.length; j ++) {
          match = match && (haystack[i+j] == needle[j]);
        }
        if (match && escapeable) {
          int c = 0; //escape characters count
          while (i - c > 0 && haystack[i - c - 1] == '\\') c++;
          return c.isEven;
        }
        return match;
    }
}

class ExpressionChunk {
  Symbol symbol;
  List<LiquidExpression> arguments;
  bool isInvokable;
  ExpressionChunk(this.symbol, [this.isInvokable = false, this.arguments = null]);
}

class LiquidExpression {
  InstanceMirror literal = null;
  List<ExpressionChunk> chain = [];
  LiquidExpression.empty();

  factory LiquidExpression.fromString(String source) {
    //replace "x + y" with "x.+(y)"
    //unary postfix	expr++    expr--    ()    []    .    ?.
    //unary prefix	++expr    --expr
    List<String> prefixes = ['-', '!', '~'];
    List<List<String>> operatorsList = [
      //multiplicative
      ['*', '/', '%', '~/'],
      //additive
      ['+', '-'],
      //shift
      ['<<', '>>'],
      //bitwise AND
      ['&'],
      //bitwise XOR
      ['^'],
      //bitwise OR
      ['|'],
      //relational and type test  as    is    is!
      ['>=', '>', '<=', '<'],
      //equality
      ['==', '!='],
      //logical AND
      ['&&'],
      //logical OR
      ['||']
      //TODO if null	??
      //TODO conditional	expr1 ? expr2 : expr3
      //TODO cascade	..
      //assignment	=    *=    /=    ~/=    %=    +=    -=    <<=    >>=    &=    ^=    |=    ??=
    ];
    source = source.trim();
    //if (source.startsWith('(') && source.endsWith(')')) return new LiquidExpression.fromString(source.substring(1, source.length - 1));

    for (var operators in operatorsList.reversed) {
      bool inString = false;
      int openingsCount = 0;
      for (int i =0; i < source.length; i++){
        if (inString && _StringExt.matches(source, '"', i, true))
          inString = false;
        else if (!inString && source[i] == '"')
          inString = true;
        if (!inString && (source[i] == '(' || source[i] == '[')) openingsCount++;
        if (!inString && (source[i] == ')' || source[i] == ']')) openingsCount--;
        if (!inString && openingsCount == 0) {
          for (var op in operators){
            if (_StringExt.matches(source, op, i)) {
              String left = source.substring(0, i);
              String right = source.substring(i + 1);
              var ret = new LiquidExpression.fromString(left);
              ret.chain.add(new ExpressionChunk(new Symbol(op), true, [new LiquidExpression.fromString(right)]));
              //print("${left}XX${right}");
              return ret;
            }
          }
        }
      }
    }
    //nie ma operatorow
    //a.b.c(x,y,z)
    var ret = new LiquidExpression.empty();
    String symbol = '';
    ExpressionChunk method = null;
    int openings = 0;
    for (int i =0; i < source.length; i++){
      if (openings == 0 && source[i]=='.'){
        //TODO float number
        ret.addChunk(symbol);
        symbol = '';
      } else if (openings == 0 && source[i]=='(') {
        method = new ExpressionChunk(new Symbol(symbol), true, new List<LiquidExpression>());
        symbol = '';
        ret.chain.add(method);
        openings ++;
      } else if (source[i]=='(') {
        symbol += source[i];
        openings ++;
      } else if (openings == 1 && (source[i] == ',' || source[i]==')')) {
        if (symbol != '' || method.arguments.length > 0) {
          method.arguments.add(new LiquidExpression.fromString(symbol));
        }
        symbol = '';
        //call return shall be the last or followed by "." and next getter
        if (source[i]==')'){
          i++;
          if (i<source.length && source[i] != '.') throw new LiquidExpressionSyntaxException('\. expected', source, i);
          openings --;
        }
      } else if (source[i]==')') {
        symbol += source[i];
        openings --;
      } else {
        symbol += source[i];
      }
    }
    if (symbol != '') ret.addChunk(symbol);

    return ret;
  }
  void addChunk(String symbol){
    if (chain.isEmpty) {
      //first occurence might be a literal
      if (symbol.startsWith('"') && symbol.endsWith('"')) {
        literal = reflect(symbol.substring(1, symbol.length - 1));
        return;
      }
      //numeric literal

      double numeric = double.parse(symbol, (e) => null);
      if (numeric != null) {
        if (symbol.indexOf('.') < -1)
          literal = reflect(numeric);
        else
          literal = reflect(numeric.round());
        return;
      }
    }
    chain.add (new ExpressionChunk(new Symbol(symbol)));
  }


  dynamic evaluate(InstanceMirror context, [Map<Symbol, InstanceMirror> localVars = null]){
    var ret = (literal != null) ? literal : context;
    bool first = true;
    for (var part in chain) {
      if (first && (localVars != null) && localVars.containsKey(part.symbol)) {
          ret = reflect(localVars[part.symbol]);
      } else {
        if (part.isInvokable) {
          //print(part.symbol);
          List args = new List();
          for (var arg in part.arguments){
            args.add(arg.evaluate(context, localVars));
          }
          ret = ret.invoke(part.symbol, args);
        } else {
          ret = ret.getField(part.symbol);
        }
      }
      first = false;
    }
    return ret.reflectee;
  }

  String toString(){
    var ret = '';
    if (literal != null) ret = '"' + literal.reflectee.toString() + '"';
    for (var part in chain){
      String name = part.symbol.toString();
      name = name.substring('Symbol("'.length, name.length - '")'.length);
      if (part.isInvokable) {
        ret = ret + '.' + name + '(' + part.arguments.map((e) => e.toString()).join(',') + ')';
      } else {
        ret = ret + '.' + name;
      }
    }
    return ret;
  }
}
