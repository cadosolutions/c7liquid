library c7liquid.test;

import 'package:test/test.dart';
import 'package:c7liquid/tokenizer.dart';


List<Object> toList(String source) {
    var ret = new List<Object>();
    var tokenizer = new Tokenizer.fromString(source);
    while (tokenizer.moveNext()) {
      ret.add(tokenizer.currentType);
      ret.add(tokenizer.current);
    }
    return ret;
}

void main() {

  group('Tokenizer', () {

    test('Tokenizer', () async {

      expect(
        toList(""),equals([
        ])
      );

      expect(
        toList("foo"),equals([
          TokenType.Text, "foo"
        ])
      );

      expect(
        toList("ąćńżź"), equals([
          TokenType.Text, "ąćńżź"
        ])
      );
      expect(
        toList("{a}foo b\\{ar{/b}"), equals([
          TokenType.Tag, "a",
          TokenType.Text, "foo b{ar",
          TokenType.Tag, "/b",
        ])
      );
      expect(
        toList("{a}{/a}"), equals([
          TokenType.Tag, "a",
          TokenType.Tag, "/a",
        ])
      );
      expect(
        toList("{a '' + 3}"), equals([
          TokenType.Tag, "a",
          TokenType.StringLiteral, "",
          TokenType.Operator, "+",
          TokenType.IntegerLiteral, "3",
        ])
      );
      expect(
        toList("{b p1}foo bar{/a x?}"), equals(
        [
          TokenType.Tag, "b",
          TokenType.Identifier, "p1",
          TokenType.Text, "foo bar",
          TokenType.Tag, "/a",
          TokenType.Identifier, "x",
          TokenType.Operator, "?",
        ]
      ));
      expect(
        toList("{b 4 * 5.0+test +foo.bar+ 'te\\s\\'t' }"), equals(
        [
          TokenType.Tag, "b",
          TokenType.IntegerLiteral, "4",
          TokenType.Operator, "*",
          TokenType.FloatLiteral, "5.0",
          TokenType.Operator, "+",
          TokenType.Identifier, "test",
          TokenType.Operator, "+",
          TokenType.Identifier, "foo",
          TokenType.Operator, ".",
          TokenType.Identifier, "bar",
          TokenType.Operator, "+",
          TokenType.StringLiteral, "tes\'t",
        ]
      ));

      expect(
        toList("foo @a.b.c baz"), equals(
        [
          TokenType.Text, "foo ",
          TokenType.InlineOp, "@",
          TokenType.Identifier, "a",
          TokenType.Operator, ".",
          TokenType.Identifier, "b",
          TokenType.Operator, ".",
          TokenType.Identifier, "c",
          TokenType.Text, " baz"
        ]
      ));

      expect(
        toList("foo @{a.x + 5} baz"), equals(
        [
          TokenType.Text, "foo ",
          TokenType.InlineOp, "@{",
          TokenType.Identifier, "a",
          TokenType.Operator, ".",
          TokenType.Identifier, "x",
          TokenType.Operator, "+",
          TokenType.IntegerLiteral, "5",
          TokenType.InlineOp, "}",
          TokenType.Text, " baz"
        ]
      ));

      /*
      expect(new Tokenizer("foo @foo.bar baz").tokens(), equals(
        ["foo ", "@", "foo", ".", "bar", " baz"]
      ));
      expect(new Tokenizer("{x 1 'abąć' 'c\\'de'}").tokens(), equals(
        ["", "{", "x", " ", "1", " ", "'", "abąć", "'", " ", "'", "c\\'de", "'", "}", ""]
      ));
      expect(new Tokenizer("foo@{x y>z}bar").tokens(), equals(
        ["foo", "@", "{", "x", " ", "y", ">", "z", "}", "bar"]
      ));
      expect(new Tokenizer("{x 'abc' > 'cde'}").tokens(), equals(
        ["", "{", "x", " ", "'", "abc", "'", " ", ">", " ", "'", "cde", "'", "}", "" ]
      ));
      expect(new Tokenizer("{x}@{x y}{/x}").tokens(), equals(
        ["", "{", "x", "}", "", "@", "{", "x", " ", "y", "}", "", "{", "/", "x", "}", ""]
      ));
      expect(new Tokenizer("{a}xxbar @asd@{agf + 5}te st{/a}").tokens(), equals(
        ["", "{", "a", "}", "xxbar ", "@", "asd", "@", "{", "agf", " ", "+", " ", "5", "}", "te st", "{", "/", "a", "}", ""]
      ));
      Stopwatch stopwatch = new Stopwatch();
      stopwatch.start();
      for (int i=0; i<10000; i++) {
          new Tokenizer("{tag p1 p2 'p3'}text node @inline.var @{inline.complex.expression + 5}{tested}another{/nested} text node{/tag}").tokens();
      }
      expect(stopwatch.elapsedMilliseconds, lessThan(1000));
      stopwatch.stop();
      */
    });
  });
}
