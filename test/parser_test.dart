library c7liquid.test;

import 'package:test/test.dart';
import 'package:c7liquid/parser.dart';
import 'package:c7liquid/tokenizer.dart';

class Child {
  int i;
  String s;
}

class Foo {
  int i;
  Float f;
  bool b;
  String s;
  Child c;
}

void main() {

  group('Parser', () {
    test('Parser', () async {

      Parser parser = new Parser<Foo>(new Tokenizer.fromString("TE{if b}@s{/if}ST"));
      parser.parse();

    });
  });
}
