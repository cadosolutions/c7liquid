library c7liquid.test;

import 'package:test/test.dart';
import 'package:c7liquid/c7liquid.dart';
import 'dart:mirrors';


@Liquid
class MyLiquidType {
  @Liquid
  String something = "something";
  MyLiquidType(this.something);
}

class DataParent {
  @Liquid
  String get className => reflect(this).type.simpleName.toString();
}

class MyData extends DataParent {

  @Liquid
  int intVar = 5;

  @Liquid
  String stringVar = "stringVarValue";

  @Liquid
  List<MyLiquidType> list = [new MyLiquidType('TIC'), new MyLiquidType('TAC'), new MyLiquidType('TOE')];

  @Liquid
  int square(int x) => x * x;
}

void main() {

  group('Basic rendering', () {
    test('File Rendering', () async {
      var x = new MyData();
      var renderer = new Renderer(await FileTemplate.getFromPath('test/templates/foo'));
      print(renderer.render(x));
      x.intVar = 3;
      x.list = [];
      print(renderer.render(x));

    });

    test('String Rendering', () {
      print(new Expression.fromString('square(test)'));
      var x = new MyData();
      var renderer = new Renderer(new StringTemplate(r"""
        <h1>Lorem</h1>
        {dump intVar}
        intVar equals @intVar and stringVar equals @stringVar
        square root of intVar equals @square(intVar)
        {if intVar>4}
          intVal is greater than 4
        {else}
          intVal is not greater than 4
        {/if}
        {if list.isEmpty}
          sorry, your list is empty
        {else}
          <ul>
          {for my in list}
            <li>@my.something</li>
          {/for}
          </ul>
        {/if}
        <pre>
        {dump}
        </pre>
      """, MyData));
      print(renderer.render(x));
      x.intVar = 3;
      x.list = [];
      print(renderer.render(x));

    });

  });
}
