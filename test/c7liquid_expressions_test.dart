library c7liquid.test;

import 'package:test/test.dart';
import 'package:c7liquid/expression.dart';

void main() {
  group('Expressions', () {
    test('Basic Math', () {
      var testExpressions = [
        ['2 + 2', 4],
        ['(1 + 3 / 9) * 3 - 4', 0],
        ['1 * 5', 5],
        //['-2', -2]
        ['(1+1)+(1+1)+(1+1)', 4]
      ];
      for (var test in testExpressions) {
        var e = new LiquidExpression.fromString(test[0]);
        expect(e.evaluate(null, {}), equals(test[1]));
      }
    });

    test('Basic Variable Usage', () {
      var testExpressions = [
        ['x * 2', {#x:2}, 4],
        ['(x + 5) * y', {#x:1, #y:2}, 12],
        ['x + y', {#x:6, #y:7}, 13],
        ['x.toUpperCase()', {#x:'test'}, 'TEST'],
        ['x.replaceRange(0, 3, "bar").toUpperCase()', {#x:'foobar'}, 'BARBAR']
      ];
      for (var test in testExpressions) {
        var e = new LiquidExpression.fromString(test[0]);
        expect(e.evaluate(null, test[1]), equals(test[2]));
      }
    });

  });
}
